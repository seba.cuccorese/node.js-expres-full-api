import mongoose from "mongoose";
const schema = mongoose.Schema;


const courseSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    author: {
        type: schema.Types.ObjectId, ref: 'User',
        required: true
    },
    description: {
        type: String,
        required: false
    },
    isEnabled: {
        type: Boolean,
        default: true
    },
    image: {
        type: String,
        required: false
    },
    students: {
        type: Number,
        default: 0
    },
    calification: {
        type: Number,
        default: 0
    }
});

export const courseModel = mongoose.model('Course', courseSchema);