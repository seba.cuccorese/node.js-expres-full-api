import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    isEnabled: {
        type: Boolean,
        default: true
    },
    image: {
        type: String,
        required: false
    }
});

export const userModel = mongoose.model('User', userSchema);