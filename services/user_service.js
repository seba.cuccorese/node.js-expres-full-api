import { Api400Error } from "../errors/Api_400_error.js";
import { userModel as User } from "../models/user_model.js";
import bcrypt from 'bcrypt';

export const saveUser = async (userRequest) => {
    const hashed = await bcrypt.hash(userRequest.password, 10);
    let user = new User({
        email: userRequest.email,
        name: userRequest.name,
        password: hashed,
    });

    return await user.save();
}

export const updateUser = async (userUpdateRequest) => {
    let userToUpdate = await User.findOneAndUpdate({ email: userUpdateRequest.email }, {
        $set: {
            name: userUpdateRequest.name,
            password: userUpdateRequest.password
        }
    });

    return await userToUpdate.save();
}

export const deleteUser = async (email) => {
    let deletedUser = await User.findOne({ email: email });

    console.log(deletedUser);
    if (!deletedUser) {
        console.error('User does not exist');
        throw new Api400Error('Invalid Email', `User with email: ${email} does not exists`);
    }
    deletedUser.isEnabled = false;

    return await deletedUser.save();
}

export const findAllUsers = async () => {
    let users = await User.find({ "isEnabled": true })
        .select({ name: 1, email: 1 });

    return users;
}

export const findByEmail = (email, callback) => {
    return User.findOne({ email: email }, callback);
}