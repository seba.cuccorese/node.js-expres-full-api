import bcrypt from 'bcrypt';


export const comparePassword = async (paramPass, dbPass) => {
   return await bcrypt.compare(paramPass, dbPass);
}