import { Api400Error } from "../errors/Api_400_error.js";
import { courseModel as Course } from "../models/course_model.js";

const validateCourseExists = (courseToUpdate) => {
    if (!courseToUpdate) {
        console.log(`Course with title: ${coursetitle} does not exists`);
        throw new Api400Error('Invalid course title', `Course with title: ${coursetitle} does not exists`);
    }
}

export const saveCourse = async (courseRequest) => {
    let course = new Course({
        title: courseRequest.body.title,
        description: courseRequest.body.description,
        author: courseRequest.user._id
    });

    return await course.save();
}

export const updateCourse = async (courseId, courseUpdateRequest) => {
    let courseToUpdate = await Course.findByIdAndUpdate(courseId, {
        $set: {
            title: courseUpdateRequest.title,
            description: courseUpdateRequest.description,
            students: courseUpdateRequest.students,
            calification: courseUpdateRequest.calification
        }
    }, { new: true });

    return await courseToUpdate.save();
}


export const deleteCourse = async (courseId) => {
    let course = await Course.findById(courseId);
    validateCourseExists(course);
    course.isEnabled = false;

    return await course.save();
}


export const listActiveCourses = async () => {
    return await Course.find({ isEnabled: true }).populate('author', 'name email -_id');
}