import express from 'express';
import mongoose from 'mongoose';
import { route as users } from './routes/users.js'
import { route as courses } from './routes/courses.js'
import { route as auth } from './routes/auth.js';
import { validateToken } from './middlewares/auth.js';
import config from 'config';
//Connection to DB;
mongoose.set('strictQuery', true)
mongoose.connect(config.get('configDB.HOST'), { useNewUrlParser: true })
    .then(() => console.log('Connected to mongo db...')).catch((e) => console.error(`Error connecting to mongo db: ${e}`));

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/api/users', validateToken, users);
app.use('/api/courses', validateToken, courses);
app.use('/api/auth', validateToken, auth)
const port = process.env.PORT || 3000;


app.listen(port, () => console.log(`API LISTENING IN PORT: ${port}`));

