import express from 'express';
import { saveUser, updateUser, deleteUser, findAllUsers, findByEmail } from '../services/user_service.js';
import { httpStatusCodes } from './http_status_codes.js';
import { validateUser } from './validations/user_validation.js';
export const route = express.Router();


route.get('/', (_req, res) => {
    let users = findAllUsers();

    users.then(users => {
        res.status(httpStatusCodes.OK).send({ users: [users] });
    }).catch(error => {
        console.error(error);
        res.status(httpStatusCodes.INTERNAL_SERVER).send({ error: error });
    })
});


route.post('/', (req, res) => {
    findByEmail(req.body.email, (err, user) => {
        if (err)
            return res.status(500).send({ error: err });
        if (user)
            return res.status(400).send({ error: 'user already exists' });
        const { error } = validateUser(req.body);
        if (error) {
            res.status(httpStatusCodes.BAB_REQUEST).send({ error: error.details });
            return;
        }
        let createdUser = saveUser(req.body);
        createdUser.then(u => {
            console.log('user created')
            res.status(httpStatusCodes.CREATED).send({ user: { name: u.name, email: u.email } });
        }).catch(e => {
            console.log(`Error creating user: ${e}`);
            res.status(500).send({ error: e });
        });
    });
})

route.put('/:email', (req, res) => {
    const { error } = validateUser(req.body);
    if (error) {
        res.status(httpStatusCodes.BAB_REQUEST).send({ error: error.details });
        return;
    }
    let updatedUser = updateUser({
        name: req.body.name,
        password: req.body.password
    });

    updatedUser.then(u => {
        console.log('User updated'),
            res.status(httpStatusCodes.OK).send({ user: { name: user.name, email: user.email } });
    }).catch(error => {
        console.error(error);
        res.status(error.statusCode).send({ error: error.description });
    });
})

route.delete('/:email', (req, res) => {
    const { error } = validateUser(req.params);
    if (error) {
        res.status(httpStatusCodes.BAB_REQUEST).send({ error: error.details });
        return;
    }
    let deletedUser = deleteUser(req.params.email);

    deletedUser.then(user => {
        console.info('User deleted successfully');
        res.status(httpStatusCodes.NO_CONTENT).send({ user: { name: user.name, email: user.email } });
    })
        .catch(error => {
            console.error(error);
            res.status(error.statusCode ? error.statusCode : 500).send(error.description ? { error: error.description } : { error: error });
        })
})