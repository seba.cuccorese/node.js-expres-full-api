import express from 'express';
import { listActiveCourses, saveCourse, deleteCourse, updateCourse } from '../services/course_service.js';
import { httpStatusCodes } from './http_status_codes.js';
import { courseValidation } from './validations/course_validation.js';
export const route = express.Router();

route.get('/', (req, res) => {
    let courses = listActiveCourses();

    courses.then(courses => {
        res.status(httpStatusCodes.OK).send({ courses: courses });
    }).catch(e => {
        console.error(e);
        res.status(httpStatusCodes.INTERNAL_SERVER).send({ error: e })
    });
});


route.post('/', (req, res) => {
    const { error } = courseValidation(req.body);
    if (error) {
        res.status(httpStatusCodes.BAB_REQUEST).send({ error: error.details });
        return;
    }
    let course = saveCourse(req);
    course.then(course => {
        console.log('Created course');
        res.status(httpStatusCodes.CREATED).send({ course: course });
    })
        .catch(e => res.status(e.statusCode ? e.statusCode : 500).send({ error: e }));
});

route.put('/:id', (req, res) => {
    const { error } = courseValidation(req.body);
    if (error) {
        res.status(httpStatusCodes.BAB_REQUEST).send({ error: error.details });
        return;
    }
    let course = updateCourse(req.params.id, req.body);
    course.then(course => {
        console.log('Course updated');
        res.status(httpStatusCodes.OK).send({ course: course });
    }).catch(e => {
        console.error(e);
        res.status(e.statusCode ? e.statusCode : 500).send({ error: e })
    });
});

route.delete('/:id', (req, res) => {
    let course = deleteCourse(req.params.id);

    course.then(course => {
        console.log('Deleted course');
        res.status(httpStatusCodes.NO_CONTENT).send({ deleted: course });
    }).catch(e => {
        console.error(e.description);
        res.status(e.statusCode ? e.statusCode : 500).send({ error: e })
    })
})