import express from "express";
import { saveUser, updateUser, deleteUser, findAllUsers, findByEmail } from '../services/user_service.js';
import { httpStatusCodes } from './http_status_codes.js';
import { comparePassword } from "../services/auth_service.js";
import jwt from 'jsonwebtoken';
import config from 'config';
export const route = express.Router();

route.post('/', (req, res) => {
    findByEmail(req.body.email)
        .then(user => {
            if (user) {
                const passwordHashed = comparePassword(req.body.password, user.password);
                passwordHashed.then(match => {
                    if (match) {
                        const jwToken = jwt.sign({ data: { _id: user._id, name: user.name, email: user.email } }, config.get('configToken.PASS'), { expiresIn: config.get('configToken.expirationTime') });
                        return res.status(httpStatusCodes.OK).send({
                            usuario: {
                                _id: user._id,
                                name: user.name,
                                email: user.email
                            }, jwToken
                        })
                    }
                    else {
                        return res.status(httpStatusCodes.BAB_REQUEST).send({ error: 'Incorrect user or password' })
                    }
                })
                    .catch(err => { return res.status(httpStatusCodes.INTERNAL_SERVER).send({ error: err }) })
            } else {
                return res.status(httpStatusCodes.BAB_REQUEST).send({ error: 'Incorrect user or password' });
            }
        })
        .catch(err => {
            res.status(httpStatusCodes.INTERNAL_SERVER).send({ error: err });
        })
});