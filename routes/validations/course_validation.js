import Joi from "joi";


const courseSchema = Joi.object({
    title: Joi.string().min(5).required(),
    description: Joi.string().min(5),
})


export const courseValidation = (course) => courseSchema.validate({ title: course.title, description: course.description });