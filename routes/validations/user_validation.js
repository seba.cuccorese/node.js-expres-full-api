import Joi from 'joi';

const userSchema = Joi.object({
    name: Joi.string().alphanum().min(3).required(),
    email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
});


export const validateUser = (user) => userSchema.validate({ name: user.name, email: user.email });