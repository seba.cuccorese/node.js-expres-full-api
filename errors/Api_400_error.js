import { httpStatusCodes } from "../routes/http_status_codes.js";
import { BaseError } from "./base_error.js";

export class Api400Error extends BaseError {
    constructor(name, description) {
        super(name, httpStatusCodes.BAB_REQUEST, description);
    }
}