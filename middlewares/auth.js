import jwt from 'jsonwebtoken';
import config from 'config';
import { httpStatusCodes } from '../routes/http_status_codes.js';

export const validateToken = (req, res, next) => {
    let token = req.get('Authorization');
    jwt.verify(token, config.get('configToken.PASS'), (err, decoded) => {
        if (err) {
            return res.status(httpStatusCodes.NOT_AUTHORIZED).send({error: err});
        }
        req.user = decoded.data;
        next();
    });
}